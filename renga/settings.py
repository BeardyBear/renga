import glob
import os, sys

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = 'a$a%pb(1w#p8!^zv0k62naws5$_5z3^t@po8kraltnp^-rs2j-'

TEST_MODE = 'test' in sys.argv

DEBUG = False

ALLOWED_HOSTS = ['.haikainoren.ga']

ROOT_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
PROJECT_NAME = os.path.basename(ROOT_PATH)
PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))

ROOT_URLCONF = 'renga.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'renga.wsgi.application'

MEDIA_ROOT = os.path.join(PROJECT_PATH, 'media')

MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(PROJECT_PATH, 'static')

STATIC_URL = '/static/'

config_files_path = os.path.join(PROJECT_PATH, 'settings', '*.conf.py')
config_files = glob.glob(config_files_path)
config_files.sort()

for f in config_files:
    execfile(os.path.abspath(f))

try:
    from local_settings import *
except ImportError:
    pass
