# -*- coding: utf-8 -*-
import tornadoredis
import tornado.web
import tornado.websocket
import tornado.ioloop
import tornado.httpclient


c = tornadoredis.Client()
c.connect()


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Content-Type', 'text/plain')
        self.write('What\'s up?')
        return True


class RengaHandler(tornado.websocket.WebSocketHandler):
    def __init__(self, *args, **kwargs):
        super(RengaHandler, self).__init__(*args, **kwargs)
        self.channels = []
        self.http_client = tornado.httpclient.AsyncHTTPClient()
        self.client = tornadoredis.Client()
        self.client.connect()

    @tornado.gen.engine
    def listen(self):
        yield tornado.gen.Task(self.client.subscribe, self.channels)
        self.client.listen(self.new_message)

    def new_message(self, message):
        self.write_message(unicode(message.body))

    def check_origin(self, origin):
        return True

    def open(self, renga_id=None):
        if renga_id:
            haiku_channel = 'renga_{0}'.format(renga_id)
            self.channels.append(haiku_channel)
        self.listen()


application = tornado.web.Application([
    (r"/test/", MainHandler),
    (r"/sock/(?:(?P<renga_id>\d+)/)?$", RengaHandler),
])
