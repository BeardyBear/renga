import datetime


JWT_AUTH = {
    'JWT_EXPIRATION_DELTA': datetime.timedelta(seconds=86400),
    'JWT_PAYLOAD_HANDLER': 'core.utils.jwt.jwt_payload_handler',
    'JWT_ALLOW_REFRESH': True
}
