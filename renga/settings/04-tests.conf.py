TEST_RUNNER = 'django.test.runner.DiscoverRunner'

if TEST_MODE:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(PROJECT_PATH, 'test_sqlite.db'),
            # 'TEST_NAME': os.path.join(ROOT_PATH, 'test_sqlite.db'),  # creates real db instead of keeping stuff in memory
        }
    }
    DEBUG = False
    TEMPLATE_DEBUG = False
    PASSWORD_HASHERS = (
        'django.contrib.auth.hashers.MD5PasswordHasher',
    )