from jwt import DecodeError
from rest_framework_jwt.serializers import User
from rest_framework_jwt.utils import jwt_decode_handler


class JWTAuthenticationMiddleware(object):
    def process_request(self, request):
        token_authorization_header = request.META.get('HTTP_AUTHORIZATION', None)

        if token_authorization_header:
            token_prefix, token_key = token_authorization_header.split(' ')
            try:
                decoded = jwt_decode_handler(token_key)
            except DecodeError:
                pass
            else:
                request.user = User.objects.get(id=decoded['id'])
