from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin


admin.autodiscover()


js_info_dict = {
    'domain': 'django',
    'packages': ('renga',),
}


APP_URLS = patterns('',
    url(r'^', include('core.urls', app_name='core', namespace='core')),
)


I18N_URLS = patterns('',
    url(r'^jsi18n/$', 'django.views.i18n.javascript_catalog', js_info_dict),
)


ADMIN_URLS = patterns('',
    url(r'^admin/', include(admin.site.urls)),
)


AUTH_URLS = patterns('',
    # url('^accounts/', include('django.contrib.auth.urls')),
    url(r'^api-token-auth/', 'rest_framework_jwt.views.obtain_jwt_token'),
    url(r'^api-token-refresh/', 'rest_framework_jwt.views.refresh_jwt_token'),
)


STATIC_URLS = static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


urlpatterns = APP_URLS + I18N_URLS + ADMIN_URLS + AUTH_URLS + STATIC_URLS


if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += patterns('', url(r'^rosetta/', include('rosetta.urls')),)
