var gulp = require('gulp');
var babel = require('gulp-babel');


gulp.task('default', ['app']);


var jsxPaths = [
        'core/static/js/app/*.jsx',
        'core/static/js/app/*/*.jsx',
        'core/static/js/app/*/*/*.jsx'
];


gulp.task("app", function () {
    return gulp.src(jsxPaths)
        .pipe(babel())
        .pipe(gulp.dest(function(file) {
            return file.base;
        }));
});
