# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from rest_framework import serializers
from core.models import Language, RengaParticipation, VerseUpvote, Verse, Renga, Profile, ChatMessage


class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = ('id', 'name', 'code')


class UserVerboseSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'profile')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'profile')


class RengaParticipationVerboseSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = RengaParticipation
        fields = ('id', 'user', 'renga',)


class RengaParticipationSerializer(serializers.ModelSerializer):
    def validate(self, data):
        renga = data['renga']

        if renga.max_participants == 0:
            return data

        participations_count = renga.participations.count()

        if participations_count + 1 > renga.max_participants:
            raise serializers.ValidationError('Participants limit reached')

        return data

    class Meta:
        model = RengaParticipation
        fields = ('id', 'user', 'renga',)


class VerseUpvoteVerboseSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = VerseUpvote
        fields = ('id', 'verse', 'user')


class VerseUpvoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = VerseUpvote
        fields = ('id', 'verse', 'user')


class VerseVerboseSerializer(serializers.ModelSerializer):
    author = UserSerializer()
    upvotes = VerseUpvoteVerboseSerializer(many=True)

    class Meta:
        model = Verse
        fields = ('id', 'renga', 'created_at', 'text', 'author', 'upvotes')


class VerseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Verse
        fields = ('id', 'renga', 'text', 'author',)


class ChatMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChatMessage
        fields = ('id', 'user', 'renga', 'text',)


class ChatMessageVerboseSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = ChatMessage
        fields = ('id', 'user', 'renga', 'text',)


class RengaVerboseSerializer(serializers.ModelSerializer):
    host = UserSerializer()
    verses = VerseVerboseSerializer(many=True)
    participations = RengaParticipationVerboseSerializer(many=True)
    chat_messages = ChatMessageVerboseSerializer(many=True)

    class Meta:
        model = Renga
        fields = ('id', 'name', 'host', 'verses', 'language', 'participations', 'is_closed', 'max_participants',
                  'chat_messages',)


class RengaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Renga
        fields = ('id', 'name', 'host', 'language', 'is_closed', 'max_participants')


class ProfileVerboseSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    hosted = serializers.SerializerMethodField()
    participated = serializers.SerializerMethodField()

    def get_hosted(self, profile):
        hosted_rengas = Renga.objects.filter(host=profile.user)
        return RengaVerboseSerializer(hosted_rengas, many=True).data

    def get_participated(self, profile):
        participated_rengas = Renga.objects.filter(participations__user=profile.user).exclude(host=profile.user)
        return RengaVerboseSerializer(participated_rengas, many=True).data

    class Meta:
        model = Profile
        fields = ('id', 'user', 'rating', 'about', 'hosted', 'participated')


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('id', 'user', 'rating', 'about',)
