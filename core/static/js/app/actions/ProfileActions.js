define([
    'jquery',
    'AppDispatcher',
    'constants/ProfileConstants'
], function ($, AppDispatcher, ProfileConstants) {
    return {
        fetchProfile: function(profileId) {
            $.ajax({
                url: '/api/profile/' + profileId,
                success: function(data) {
                    AppDispatcher.dispatch({
                        actionType: ProfileConstants.PROFILE_FETCH_SUCCESS,
                        data: data
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    AppDispatcher.dispatch({
                        actionType: ProfileConstants.PROFILE_FETCH_FAIL,
                        data: jqXHR.responseJSON
                    });
                }
            });
        },
        editProfile: function(profile) {
            $.ajax({
                method: 'POST',
                url: '/api/profile/' + profile.id + '/update/',
                data: profile,
                success: function (data) {
                    AppDispatcher.dispatch({
                        actionType: ProfileConstants.PROFILE_EDIT_SUCCESS,
                        data: data
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    AppDispatcher.dispatch({
                        actionType: ProfileConstants.PROFILE_EDIT_FAIL,
                        data: jqXHR.responseJSON
                    });
                }
            });
        }
    };
});