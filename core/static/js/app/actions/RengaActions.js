define(['AppDispatcher', 'constants/RengaConstants', '../stores/Auth'], function (AppDispatcher, RengaConstants, Auth) {
    return {
        createRenga: function(renga) {
            $.ajax({
                method: 'POST',
                url: '/api/renga/create/',
                data: renga,
                success: function (data) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.RENGA_CREATE_SUCCESS,
                        data: data
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.RENGA_CREATE_FAIL,
                        data: jqXHR.responseJSON
                    });
                }
            });
        },
        editRenga: function(renga) {
            $.ajax({
                method: 'PATCH',
                url: '/api/renga/' + renga.id + '/update/',
                data: renga,
                success: function (data) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.RENGA_EDIT_SUCCESS,
                        data: data
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.RENGA_EDIT_FAIL,
                        data: jqXHR.responseJSON
                    });
                }
            });
        },
        fetchRenga: function(rengaId) {
            $.ajax({
                url: '/api/renga/' + rengaId,
                success: function(data) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.RENGA_FETCH_SUCCESS,
                        data: data
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.RENGA_FETCH_FAIL,
                        data: jqXHR.responseJSON
                    });
                }
            });
        },
        postVerse: function(rengaId, verseText) {
            $.ajax({
                method: 'POST',
                url: '/api/verse/create/',
                data: {
                    renga: rengaId,
                    text: verseText,
                    author: Auth.getUserId()
                },
                success: function(data) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.VERSE_POST_SUCCESS,
                        data: data
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.VERSE_POST_FAIL,
                        data: errorThrown
                    });
                }
            });
        },
        upvoteVerse: function(verseId) {
            $.ajax({
                method: 'POST',
                url: '/api/upvote/create/',
                data: {
                    verse: verseId,
                    user: Auth.getUserId()
                },
                success: function(data) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.VERSE_UPVOTE_SUCCESS,
                        data: data
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.VERSE_UPVOTE_FAIL,
                        data: errorThrown
                    });
                }
            });
        },
        fetchChatMessages: function(rengaId) {
            $.ajax({
                url: '/api/chat/?rengaId=' + rengaId,
                success: function(data) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.CHAT_MESSAGES_FETCH_SUCCESS,
                        data: data
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.CHAT_MESSAGES_FETCH_FAIL,
                        data: jqXHR.responseJSON
                    });
                }
            });
        },
        postChatMessage: function(rengaId, text) {
            $.ajax({
                method: 'POST',
                url: '/api/chat/create/',
                data: {
                    renga: rengaId,
                    text: text,
                    user: Auth.getUserId()
                },
                success: function(data) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.CHAT_MESSAGE_POST_SUCCESS,
                        data: data
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.CHAT_MESSAGE_POST_FAIL,
                        data: errorThrown
                    });
                }
            });
        },
        joinRenga: function(rengaId) {
            $.ajax({
                method: 'POST',
                url: '/api/participation/create/',
                data: {
                    renga: rengaId,
                    user: Auth.getUserId()
                },
                success: function(data) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.RENGA_JOIN_SUCCESS,
                        data: data
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.RENGA_JOIN_FAIL,
                        data: errorThrown
                    });
                }
            });
        },
        leaveRenga: function(participationId) {
            $.ajax({
                method: 'DELETE',
                url: '/api/participation/destroy/' + participationId,
                success: function(data) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.RENGA_LEAVE_SUCCESS,
                        data: data
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.RENGA_LEAVE_FAIL,
                        data: errorThrown
                    });
                }
            });
        },
        fetchPage: function (page, name, language) {
            var params = $.param({page: page || 1, name: name || '', language: language || null});
            $.ajax({
                url: '/api/renga/?' + params,
                success: function (data) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.PAGE_FETCH_SUCCESS,
                        data: data
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    AppDispatcher.dispatch({
                        actionType: RengaConstants.PAGE_FETCH_FAIL,
                        data: errorThrown
                    });
                }
            });
        }
    };
});