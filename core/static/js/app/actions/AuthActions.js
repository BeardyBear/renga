define([
    'jquery',
    'AppDispatcher',
    'constants/AuthConstants'
], function ($, AppDispatcher, AuthConstants) {
    return {
        register: function(username, email, password1, password2) {
            $.ajax({
                method: 'POST',
                url: '/auth/register/',
                data: {
                    username: username,
                    email: email,
                    password1: password1,
                    password2: password2
                },
                success: function (data) {
                    AppDispatcher.dispatch({
                        actionType: AuthConstants.REGISTER_SUCCESS,
                        data: data
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    AppDispatcher.dispatch({
                        actionType: AuthConstants.REGISTER_FAIL,
                        data: jqXHR.responseJSON
                    });
                }
            });
        },
        changePassword: function(oldPassword, password1, password2) {
            $.ajax({
                method: 'POST',
                url: '/auth/password/change/',
                data: {
                    old_password: oldPassword,
                    new_password1: password1,
                    new_password2: password2
                },
                success: function (data) {
                    AppDispatcher.dispatch({
                        actionType: AuthConstants.PASSWORD_CHANGE_SUCCESS,
                        data: data
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    AppDispatcher.dispatch({
                        actionType: AuthConstants.PASSWORD_CHANGE_FAIL,
                        data: jqXHR.responseJSON
                    });
                }
            });
        },
        resetPassword: function(email) {
            $.ajax({
                method: 'POST',
                url: '/auth/password/reset/',
                data: {
                    email: email
                },
                success: function (data) {
                    AppDispatcher.dispatch({
                        actionType: AuthConstants.PASSWORD_RESET_SUCCESS,
                        data: data
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    AppDispatcher.dispatch({
                        actionType: AuthConstants.PASSWORD_RESET_FAIL,
                        data: jqXHR.responseJSON
                    });
                }
            });
        },
        confirmPasswordReset: function(password1, password2, uidb64, token) {
            $.ajax({
                method: 'POST',
                url: '/auth/password/reset/confirm/',
                data: {
                    new_password1: password1,
                    new_password2: password2,
                    uidb64: uidb64,
                    token: token
                },
                success: function (data) {
                    AppDispatcher.dispatch({
                        actionType: AuthConstants.PASSWORD_RESET_CONFIRM_SUCCESS,
                        data: data
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    AppDispatcher.dispatch({
                        actionType: AuthConstants.PASSWORD_RESET_CONFIRM_FAIL,
                        data: jqXHR.responseJSON
                    });
                }
            });
        },
        logIn: function(username, password) {
            $.ajax({
                method: 'POST',
                url: '/api-token-auth/',
                data: {
                    username: username,
                    password: password
                },
                success: function (data) {
                    AppDispatcher.dispatch({
                        actionType: AuthConstants.LOG_IN_SUCCESS,
                        data: data
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    AppDispatcher.dispatch({
                        actionType: AuthConstants.LOG_IN_FAIL,
                        data: jqXHR.responseJSON
                    });
                }
            });
        },
        logOut: function() {
            AppDispatcher.dispatch({
                actionType: AuthConstants.LOG_OUT
            });
        },
        refreshToken: function(token) {
            $.ajax({
                method: 'POST',
                url: '/api-token-refresh/',
                data: {
                    token: token
                },
                success: function (data) {
                    AppDispatcher.dispatch({
                        actionType: AuthConstants.JWT_REFRESH_SUCCESS,
                        data: data
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    AppDispatcher.dispatch({
                        actionType: AuthConstants.JWT_REFRESH_FAIL,
                        data: jqXHR.responseJSON
                    });
                }
            });
        }
    };
});