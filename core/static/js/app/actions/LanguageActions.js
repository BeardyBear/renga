define(['AppDispatcher', 'constants/LanguageConstants'], function (AppDispatcher, LanguageConstants) {
    return {
        fetch: function () {
            $.ajax({
                url: '/api/languages/',
                success: function (data) {
                    AppDispatcher.dispatch({
                        actionType: LanguageConstants.LANGUAGES_FETCH,
                        languages: data
                    });
                }
            });
        }
    };
});