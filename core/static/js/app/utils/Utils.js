define([
    'jquery'
], function ($) {
    function enabled(selector) {
        return !$(selector).attr('disabled');
    }

    function disable(selector, timeout, opacity) {
        if (!enabled(selector)) {
            return false;
        }

        var el = $(selector);
        el.attr('disabled', true).fadeTo(timeout || 100, opacity || 0.3);
        el.append('<div class="widget-box-overlay"><i class="fa fa-spinner fa-spin fa-2x white"></i></div>');
    }

    function enable(selector, timeout, opacity) {
        if (enabled(selector)) {
            return false;
        }

        var el = $(selector);
        el.attr('disabled', false).fadeTo(timeout || 100, opacity || 1);
        el.find('.widget-box-overlay').remove();
    }

    return {
        enabled: enabled,
        disable: disable,
        enable: enable
    }
});