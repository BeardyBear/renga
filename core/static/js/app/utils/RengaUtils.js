define(['underscore'], function (_) {
    return {
        getParticipation: function (renga, userId) {
            return _.find(renga.participations, function (participation) {
                return participation.user.id == userId;
            });
        }
    };
});