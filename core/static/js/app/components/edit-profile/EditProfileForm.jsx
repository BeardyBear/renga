define([
    'react',
    'router',
    'stores/ProfileStore',
    'actions/ProfileActions',
    'constants/ProfileConstants',
    'components/FieldErrors',
    'utils/Utils'
], function (React, Router, ProfileStore, ProfileActions, ProfileConstants, FieldErrors, Utils) {
    return React.createClass({
        getInitialState: function () {
            return {
                profile: {},
                errors: {}
            };
        },
        componentWillMount: function() {
            ProfileActions.fetchProfile(this.props.params.profileId);

            ProfileStore.addEventListener(ProfileConstants.PROFILE_FETCH_SUCCESS, this._onFetch);
            //ProfileStore.addEventListener(ProfileConstants.PROFILE_FETCH_FAIL, this._onFetchFail);
            ProfileStore.addEventListener(ProfileConstants.PROFILE_EDIT_SUCCESS, this._onSuccess);
            ProfileStore.addEventListener(ProfileConstants.PROFILE_EDIT_FAIL, this._onFail);
        },
        componentWillUnmount: function() {
            ProfileStore.removeEventListener(ProfileConstants.PROFILE_FETCH_SUCCESS, this._onFetch);
            ProfileStore.removeEventListener(ProfileConstants.PROFILE_EDIT_SUCCESS, this._onSuccess);
            ProfileStore.removeEventListener(ProfileConstants.PROFILE_EDIT_FAIL, this._onFail);
            ProfileStore.reset();
        },
        _onFetch: function() {
            this.setState({profile: ProfileStore.getProfile()});
        },
        _onSuccess: function() {
            this.props.history.pushState(null, '/profile/' + this.state.profile.id);
        },
        _onFail: function() {
            this.setState({errors: ProfileStore.getErrors()}, Utils.enable('.form-control'));
        },
        handleSubmit: function(e) {
            e.preventDefault();

            Utils.disable('.form-control');

            ProfileActions.editProfile(this.state.profile);
        },
        _onAboutChange: function(e) {
            var profile = this.state.profile;
            profile.about = e.target.value;
            this.setState({profile: profile});
        },
        render: function() {
            return (
                <div>
                    <h1>Edit your profile</h1>
                    <form className="form-horizontal" onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <textarea className="form-control" value={this.state.profile.about}
                                      onChange={this._onAboutChange} placeholder="Tell us about yourself" rows="10" />
                            <FieldErrors errors={this.state.errors.about} />
                        </div>
                        <button type="submit" className="btn btn-default pull-right">Submit</button>
                    </form>
                </div>
            );
        }
    });
});