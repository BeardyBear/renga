define([
    'react',
    'router',
    'jquery',
    'stores/Auth',
    'components/chat/ChatForm',
    'stores/RengaStore',
    'constants/RengaConstants',
    'mCustomScrollbar'
], function (React, Router, $, Auth, ChatForm, RengaStore, RengaConstants) {
    return React.createClass({
        getInitialState: function () {
            return {
                chatMessages: []
            };
        },
        componentWillMount: function() {
            RengaStore.addEventListener(RengaConstants.RENGA_FETCH_SUCCESS, this._onRengaChange);
            RengaStore.addEventListener(RengaConstants.RENGA_CHANGE, this._onRengaChange);
            RengaStore.addEventListener(RengaConstants.CHAT_MESSAGE_POST_SUCCESS, this._onRengaChange);
        },
        componentWillUnmount: function() {
            RengaStore.removeEventListener(RengaConstants.RENGA_FETCH_SUCCESS, this._onRengaChange);
            RengaStore.removeEventListener(RengaConstants.RENGA_CHANGE, this._onRengaChange);
            RengaStore.removeEventListener(RengaConstants.CHAT_MESSAGE_POST_SUCCESS, this._onRengaChange);
        },
        componentDidMount: function() {
            $('.ctext').mCustomScrollbar({
                theme: 'dark',
                setHeight: '200px',
                callbacks: {
                    onInit: function() {
                        $(this).mCustomScrollbar('scrollTo', 'bottom');
                    },
                    onUpdate: function() {
                        $(this).mCustomScrollbar('scrollTo', 'bottom');
                    }
                }
            });
        },
        _onRengaChange: function() {
            var renga = RengaStore.getRenga();
            if (renga.chat_messages != this.state.chatMessages) {
                this.setState({chatMessages: renga.chat_messages});
            }
        },
        render: function() {
            var messages = [];

            _.each(this.state.chatMessages, function(message, index) {
                messages.push(
                    <div key={index}>
                        <div className="chat-message">
                            <span className="nick">
                                <Router.Link to={'/profile/' + message.user.profile}>
                                    {message.user.username}
                                </Router.Link>
                            </span>
                            <span className="message">{message.text}</span>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                )
            });

            var bottom;
            if (Auth.isAuthenticated()) {
                bottom = <ChatForm rengaId={this.props.params.rengaId} />;
            } else {
                bottom = <p className="alert alert-info renga-warning">Log in to post your messages</p>;
            }

            return (
                <div className="chat">
                    <div className="ctop"></div>
                    <div className="cshad">
                        <div className="cpaper">
                            <div className="cpaperw">
                                <h2>chat</h2>
                                <div className="ctext" id="messages-container">
                                    <div>
                                        {messages}
                                    </div>
                                </div>
                                {bottom}
                            </div>
                        </div>
                    </div>
                    <div className="cbottom"></div>
                </div>
            );
        }
    });
});