define([
    'react',
    'stores/RengaStore',
    'actions/RengaActions',
    'constants/RengaConstants'
], function (React, RengaStore, RengaActions, RengaConstants) {
    return React.createClass({
        getInitialState: function() {
            return {text: ''};
        },
        componentWillMount: function() {
            RengaStore.addEventListener(RengaConstants.CHAT_MESSAGE_POST_SUCCESS, this._onChatMessagePostSuccess);
        },
        componentWillUnmount: function() {
            RengaStore.removeEventListener(RengaConstants.CHAT_MESSAGE_POST_SUCCESS, this._onChatMessagePostSuccess);
        },
        handleTextChange: function (e) {
            this.setState({text: e.target.value});
        },
        _onChatMessagePostSuccess: function() {
            this.setState({text: ''});
        },
        handleSubmit: function(e) {
            e.preventDefault();

            if (this.state.text) {
                RengaActions.postChatMessage(this.props.rengaId, this.state.text);
            }
        },
        render: function() {
            return (
                <form className="form-horizontal" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <input className="form-control" type="text" placeholder="Message"
                               onChange={this.handleTextChange} value={this.state.text} />
                    </div>
                    <button type="submit" className="pull-right"></button>
                </form>
            );
        }
    });
});