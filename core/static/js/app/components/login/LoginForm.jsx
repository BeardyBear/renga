define([
    'react',
    'router',
    'ReactDOM',
    'stores/Auth',
    'actions/AuthActions',
    'constants/AuthConstants',
    'components/FieldErrors',
    'utils/Utils'
], function (React, Router, ReactDOM, Auth, AuthActions, AuthConstants, FieldErrors, Utils) {
    return React.createClass({
        getInitialState: function () {
            return {
                errors: {}
            };
        },
        componentWillMount: function() {
            Auth.addEventListener(AuthConstants.LOG_IN_SUCCESS, this._onSuccess);
            Auth.addEventListener(AuthConstants.LOG_IN_FAIL, this._onFail);
        },
        componentWillUnmount: function() {
            Auth.removeEventListener(AuthConstants.LOG_IN_SUCCESS, this._onSuccess);
            Auth.removeEventListener(AuthConstants.LOG_IN_FAIL, this._onFail);
        },
        handleSubmit: function(e) {
            e.preventDefault();

            Utils.disable('.form-control');

            var username = ReactDOM.findDOMNode(this.refs.username).value.trim();
            var password = ReactDOM.findDOMNode(this.refs.password).value.trim();

            AuthActions.logIn(username, password);
        },
        _onSuccess: function() {
            if (this.props.location.query.next) {
                this.props.history.pushState(null, this.props.location.query.next);
            } else {
                this.props.history.pushState(null, '/');
            }
        },
        _onFail: function() {
            var errors = Auth.getErrors(AuthConstants.LOG_IN_FAIL);
            this.setState({errors: errors}, Utils.enable('.form-control'));
        },
        render: function() {
            return (
                <div className="row">
                    <h1>Log in</h1>
                    <form className="form-horizontal" onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <input className="form-control" id="username" ref="username" type="text"
                                   placeholder="Username" />
                            <FieldErrors errors={this.state.errors.username} />
                        </div>
                        <div className="form-group">
                            <input className="form-control" id="password" ref="password" type="password"
                                   placeholder="Password" />
                            <FieldErrors errors={this.state.errors.password} />
                            <FieldErrors errors={this.state.errors.non_field_errors} />
                        </div>
                        <div className="row">
                            <div className="col-lg-8 auth-extra-links">
                                <Router.Link to="/register">I want to register</Router.Link> <br />
                                <Router.Link to="/password/reset">I want to reset my password</Router.Link >
                            </div>
                            <div className="col-lg-4">
                                <button type="submit" className="btn btn-default pull-right">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            );
        }
    });
});