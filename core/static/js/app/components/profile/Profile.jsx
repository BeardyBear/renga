define([
    'react',
    'router',
    'stores/Auth',
    'actions/ProfileActions',
    'stores/ProfileStore',
    'constants/ProfileConstants',
    'components/profile/RengaList'
], function (React, Router, Auth, ProfileActions, ProfileStore, ProfileConstants, RengaList) {
    return React.createClass({
        getInitialState: function() {
            return {
                profile: {}
            };
        },
        componentWillMount: function() {
            ProfileActions.fetchProfile(this.props.params.profileId);
            ProfileStore.addEventListener(ProfileConstants.PROFILE_FETCH_SUCCESS, this._onProfileFetchSuccess);
        },
        componentWillUnmount: function() {
            ProfileStore.removeEventListener(ProfileConstants.PROFILE_FETCH_SUCCESS, this._onProfileFetchSuccess);
            ProfileStore.reset();
        },
        _onProfileFetchSuccess: function() {
            var profile = ProfileStore.getProfile();
            this.setState({profile: profile});
        },
        render: function() {
            var profile = this.state.profile;
            var user = profile.user || {};

            var about = profile.about || 'Author hadn\'t written anything here yet';

            var editButton, actions;

            if (user.id == Auth.getUserId()) {
                var editUrl = '/profile/' + this.state.profile.id + '/edit/';
                editButton = (
                    <Router.Link to={editUrl}>
                        <img src="/static/img/brush.png" alt="Edit" />
                    </Router.Link>
                );

                actions = (
                    <div className="row">
                        <h1>Actions</h1>

                        <Router.Link to="/password/change/">I want to change my password</Router.Link>
                    </div>
                );
            }

            return (
                <div>
                    <div className="row">
                        <h1>author profile {editButton}</h1>

                        <p className="nickname">Nickname: {user.username}</p>

                        <p className="rating">Rating: {profile.rating} </p>
                    </div>

                    <div className="row">
                        <h1>About author</h1>

                        <p>{about}</p>
                    </div>

                    {actions}

                    <div className="row">
                        <div className="col-lg-6">
                            <h1 translate>hosted</h1>

                            <RengaList rengas={profile.hosted} />
                        </div>

                        <div className="col-lg-6">
                            <h1 translate>participated</h1>

                            <RengaList rengas={profile.participated} />
                        </div>
                    </div>
                </div>
            );
        }
    });
});