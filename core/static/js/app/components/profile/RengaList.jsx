define(['react', 'router'], function (React, Router) {
    return React.createClass({
        propTypes: {
            rengas: React.PropTypes.array
        },
        getDefaultProps: function() {
            return {
                rengas: []
            }
        },
        render: function() {
            var output;

            if (this.props.rengas.length) {
                var listItems = this.props.rengas.map(function(renga) {
                    var rengaUrl = '/renga/' + renga.id;
                    return (
                        <dd key={renga.id}>
                            <Router.Link to={rengaUrl}>
                                {renga.name}
                            </Router.Link>
                        </dd>
                    );
                });
                output = <dl className="renga-list">{listItems}</dl>;
            } else {
                output = <span>No rengas to show</span>;
            }

            return (
                <div>{output}</div>
            );
        }
    });
});