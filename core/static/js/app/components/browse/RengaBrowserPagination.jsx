define(['react', 'classNames'], function (React, classNames) {
    return React.createClass({
        propTypes: {
            paginationBundle: React.PropTypes.object,
            fetchPage: React.PropTypes.func
        },
        _onPageLinkClick: function(e) {
            e.preventDefault();
            var pageVal = e.target.getAttribute('data-page');
            this.props.fetchPage(pageVal);
        },
        _onPreviousClick: function(e) {
            e.preventDefault();
            this.props.fetchPage(this.props.paginationBundle.number - 1);
        },
        _onNextClick: function(e) {
            e.preventDefault();
            this.props.fetchPage(this.props.paginationBundle.number + 1);
        },
        render: function() {
            var previous, pageLinks = [], next;

            if (this.props.paginationBundle.previous) {
                previous = (
                    <li>
                        <a href="#" onClick={this._onPreviousClick}>
                            <span>«</span>
                        </a>
                    </li>
                );
            }

            if (this.props.paginationBundle.next) {
                next = (
                    <li>
                        <a href="#" onClick={this._onNextClick}>
                            <span>»</span>
                        </a>
                    </li>
                );
            }

            if (this.props.paginationBundle.num_pages > 1) {
                for (var i = 1; i - 1 < this.props.paginationBundle.num_pages; i++) {
                    var classes = classNames({
                        active: i == this.props.paginationBundle.number
                    });
                    pageLinks.push(
                        <li key={i} className={classes}>
                            <a href="#" data-page={i} onClick={this._onPageLinkClick}>
                                {i}
                            </a>
                        </li>
                    );
                }
            }

            return (
                <ul className="pagination">
                    {previous}
                    {pageLinks}
                    {next}
                </ul>
            );
        }
    });
});