define([
    'react',
    'underscore',
    'components/LanguageSelect'
], function (React, _, LanguageSelect) {
    return React.createClass({
        propTypes: {
            onFiltersChange: React.PropTypes.func
        },
        getInitialState: function () {
            return {
                name: null,
                language: null
            };
        },
        componentWillMount: function() {
            this._nameInputChange = _.debounce(this._nameInputChange, 500);
        },
        _onLanguageChange: function(languageId) {
            this.setState({language: languageId}, this.onFiltersChange);
        },
        _onNameInputChange: function(e) {
            this.setState({name: e.target.value}, this.onFiltersChange);
        },
        onFiltersChange: function() {
            this.props.onFiltersChange(this.state);
        },
        render: function() {
            return (
                <form className="form-horizontal">
                    <div className="form-group">
                        <input className="form-control" id="name" ref="name" type="text" placeholder="Name"
                               onChange={this._onNameInputChange} />
                    </div>
                    <div className="form-group">
                        <LanguageSelect id="language" onChange={this._onLanguageChange} selected={this.state.language} />
                    </div>
                </form>
            );
        }
    });
});