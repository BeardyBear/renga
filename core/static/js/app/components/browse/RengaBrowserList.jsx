define(['react', 'router'], function (React, Router) {
    return React.createClass({
        propTypes: {
            paginationBundle: React.PropTypes.object
        },
        render: function() {
            var rengas = this.props.paginationBundle.results, rengasList, noResultsMessage;

            if (rengas && rengas.length) {
                var rengasListItems = rengas.map(function (renga) {
                    return (
                        <dd key={renga.id}>
                            <Router.Link to={'/renga/' + renga.id}>
                                {renga.name}
                            </Router.Link>
                        </dd>
                    )
                });
                rengasList = <dl className="renga-list">{rengasListItems}</dl>;
            } else if (typeof rengas != 'undefined') {
                noResultsMessage = (
                    <p className="alert alert-info">Sorry, no matching rengas were found</p>
                )
            }

            return (
                <div>
                    {rengasList}
                    {noResultsMessage}
                </div>
            );
        }
    });
});