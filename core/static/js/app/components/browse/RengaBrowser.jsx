define([
    'react',
    'router',
    'stores/RengaPaginationStore',
    'actions/RengaActions',
    'constants/RengaConstants',
    'components/browse/RengaBrowserFiltersForm',
    'components/browse/RengaBrowserPagination',
    'components/browse/RengaBrowserList'
], function (React, Router, RengaPaginationStore, RengaActions, RengaConstants, RengaBrowserFiltersForm,
             RengaBrowserPagination, RengaBrowserList) {
    return React.createClass({
        getInitialState: function() {
            return {
                paginationBundle: {},
                filters: {}
            };
        },
        componentWillMount: function() {
            RengaPaginationStore.addEventListener(RengaConstants.PAGE_FETCH_SUCCESS, this._onPageFetch);
        },
        componentWillUnmount: function() {
            RengaPaginationStore.removeEventListener(RengaConstants.PAGE_FETCH_SUCCESS, this._onPageFetch);
        },
        shouldComponentUpdate: function(nextProps, nextState) {
            return this.state.paginationBundle != nextState.paginationBundle;
        },
        _onPageFetch: function () {
            this.setState({paginationBundle: RengaPaginationStore.getPaginationBundle()});
        },
        _onFiltersChange: function(filters) {
            this.setState({filters: filters}, this.fetchPage)
        },
        fetchPage: function(pageVal) {
            RengaActions.fetchPage(pageVal || 1, this.state.filters.name, this.state.filters.language);
        },
        render: function() {
            return (
                <div>
                    <h1>All haikai no renga</h1>

                    <RengaBrowserFiltersForm onFiltersChange={this._onFiltersChange} />

                    <div>
                        <RengaBrowserList paginationBundle={this.state.paginationBundle} />
                    </div>

                    <div>
                        <RengaBrowserPagination fetchPage={this.fetchPage} paginationBundle={this.state.paginationBundle} />
                    </div>
                </div>
            );
        }
    });
});