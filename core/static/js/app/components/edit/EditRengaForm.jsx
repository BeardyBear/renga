define([
    'react',
    'router',
    'stores/Auth',
    'actions/RengaActions',
    'constants/RengaConstants',
    'stores/RengaStore',
    'components/LanguageSelect',
    'components/FieldErrors'
], function (React, Router, Auth, RengaActions, RengaConstants, RengaStore, LanguageSelect, FieldErrors) {
    return React.createClass({
        getInitialState: function () {
            return {
                renga: {},
                errors: {}
            };
        },
        componentWillMount: function() {
            RengaActions.fetchRenga(this.props.params.rengaId);

            RengaStore.addEventListener(RengaConstants.RENGA_FETCH_SUCCESS, this._onFetch);
            //RengaStore.addEventListener(RengaConstants.RENGA_FETCH_FAIL, this._onFetchFail);
            RengaStore.addEventListener(RengaConstants.RENGA_EDIT_SUCCESS, this._onSuccess);
            RengaStore.addEventListener(RengaConstants.RENGA_EDIT_FAIL, this._onFail);
        },
        componentWillUnmount: function() {
            RengaStore.removeEventListener(RengaConstants.RENGA_FETCH_SUCCESS, this._onFetch);
            RengaStore.removeEventListener(RengaConstants.RENGA_EDIT_SUCCESS, this._onSuccess);
            RengaStore.removeEventListener(RengaConstants.RENGA_EDIT_FAIL, this._onFail);
            RengaStore.reset();
        },
        _onFetch: function() {
            this.setState({renga: RengaStore.getRenga()});
        },
        _onSuccess: function() {
            this.props.history.pushState(null, '/renga/' + this.state.renga.id);
        },
        _onFail: function() {
            this.setState({errors: RengaStore.getErrors()});
        },
        handleSubmit: function(e) {
            e.preventDefault();

            RengaActions.editRenga(this.state.renga);
        },
        _onNameChange: function(e) {
            var renga = this.state.renga;
            renga.name = e.target.value;
            this.setState({renga: renga});
        },
        _onLanguageChange: function(languageId) {
            var renga = this.state.renga;
            renga.language = languageId;
            this.setState({renga: renga});
        },
        render: function() {
            return (
                <div>
                    <h1>Edit renga</h1>
                    <form className="form-horizontal" onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <input className="form-control" id="name" ref="name" type="text" placeholder="Name"
                                   value={this.state.renga.name} onChange={this._onNameChange} />
                            <FieldErrors errors={this.state.errors.name} />
                        </div>
                        <div className="form-group">
                            <LanguageSelect id="language" onChange={this._onLanguageChange} selected={this.state.renga.language} />
                        </div>
                        <button type="submit" className="btn btn-default pull-right">Submit</button>
                    </form>
                </div>
            );
        }
    });
});