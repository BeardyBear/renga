define([
    'react',
    'router',
    'stores/Auth',
    'actions/AuthActions',
    'constants/AuthConstants'
], function (React, Router, Auth, AuthActions, AuthConstants) {
    return React.createClass({
        componentWillMount: function() {
            Auth.addEventListener(AuthConstants.LOG_OUT, this._onLogOut);
            AuthActions.logOut();
        },
        componentWillUnmount: function() {
            Auth.removeEventListener(AuthConstants.LOG_OUT, this._onLogOut);
        },
        _onLogOut: function() {
            if (this.props.location.query.next) {
                this.props.history.pushState(null, this.props.location.query.next);
            } else {
                this.props.history.pushState(null, '/');
            }
        },
        render: function() {
            return <p>You are now logged out</p>;
        }
    });
});