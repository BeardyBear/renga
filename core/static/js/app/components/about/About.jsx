define(['react'], function (React) {
    return React.createClass({
        render: function() {
            return (
                <div className="about">
                    <h1>What is haikai no renga?</h1>

                    <p>Renku (連句 "linked verses"), or haikai no renga (俳諧の連歌, "comic linked verse"), is a Japanese form
                        of popular
                        collaborative linked verse poetry. It is a development of the
                        older Japanese poetic tradition of ushin renga, or orthodox collaborative linked verse. At renku
                        gatherings
                        participating poets take turns providing alternating verses of 17 and 14 morae.
                        Initially haikai no renga distinguished itself through vulgarity and coarseness of wit, before
                        growing into a
                        legitimate artistic tradition, and eventually giving birth to the haiku form of
                        Japanese poetry. The term renku gained currency after 1904, when Kyoshi Takahama started to use
                        it.</p>

                    <p>The oldest known collection of haikai linked verse appears in the first imperial anthology of
                        renga, the
                        Tsukubashu (1356-57).</p>

                    <p>Traditional renga was a group activity in which each participant displayed his wit by
                        spontaneously composing a
                        verse in response to the verse that came before; the more
                        interesting the relationship between the two verses the more impressive the poet’s ability. The
                        links between
                        verses could range from vulgar to artistic, but as renga was taken up by skilled
                        poets and developed into a set form, the vulgarity of its early days came to be ignored.</p>

                    <p>Haikai no renga, in response to the stale set forms that preceded it, embraced this vulgar
                        attitude and was
                        typified by contempt for traditional poetic and cultural ideas, and by
                        the rough, uncultured language that it used. The haikai spirit, as it came to be called,
                        embraced the natural
                        humor that came from the combination of disparate elements. To that end haikai
                        poets would often combine elements of traditional poems with new ones they created. A well-known
                        example of this
                        early attitude is the opening couplet, possibly by Yamazaki Sōkan
                        (1464–1552), from his Inutsukubashū (犬筑波集, "Mongrel Renga Collection").</p>

                    <p>He was given the following prompt:</p>
                    <blockquote>
                        kasumi no koromo suso wa nurekeri<br/>
                        <span>The robe of haze is wet at its hem</span>
                    </blockquote>
                    <p>to which he responded:</p>
                    <blockquote>
                        saohime no haru tachi nagara shito o shite<br/>
                        <span>Princess Sao of spring pissed as she started</span>
                    </blockquote>
                    <p>This poem clearly derives its humor from shock value. Never before in Japanese culture had anyone
                        dared to talk
                        of the goddess of spring in such a manner. Taking an ostensibly
                        traditional and poetic prompt and injecting vulgar humor while maintaining the connection of the
                        damp hems and
                        the spring mists was exactly the sort of thing that early haikai poets were
                        known for.</p>

                    <p>A comparable, though less evolved, tradition of 'linked verse' (lién jù, written with the same
                        characters as
                        'renku') evolved in Qin-dynasty China, and it has been argued that
                        this Chinese form influenced Japanese renga during its formative period.</p>
                </div>
            );
        }
    });
});