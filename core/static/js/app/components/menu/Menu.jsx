define(['react', 'router', 'stores/Auth'], function (React, Router, Auth) {
    return React.createClass({
        render: function() {
            var Link = Router.Link;

            var activeLinks;

            if (Auth.isAuthenticated()) {
                var profileHref = '/profile/' + Auth.getUser().profile;
                activeLinks = [
                    <li key="home">
                        <Router.IndexLink to="/">
                            <span className="icon icon-home"></span> Home
                        </Router.IndexLink>
                    </li>,
                    <li key="browse">
                        <Link to="/browse">
                            <span className="icon icon-browse"></span> Browse
                        </Link>
                    </li>,
                    <li key="about">
                        <Link to="/about">
                            <span className="icon icon-about"></span> About
                        </Link>
                    </li>,
                    <li key="profile">
                        <Link to={profileHref}>
                            <span className="icon icon-register"></span> Profile
                        </Link>
                    </li>,
                    <li key="exit">
                        <Link to="/logout" query={{next: window.location.pathname}}>
                            <span className="icon icon-login"></span> Log out
                        </Link>
                    </li>
                ]
            } else {
                activeLinks = [
                    <li key="home">
                        <Router.IndexLink to="/">
                            <span className="icon icon-home"></span> Home
                        </Router.IndexLink>
                    </li>,
                    <li key="browse">
                        <Link to="/browse">
                            <span className="icon icon-browse"></span> Browse
                        </Link>
                    </li>,
                    <li key="about">
                        <Link to="/about">
                            <span className="icon icon-about"></span> About
                        </Link>
                    </li>,
                    <li key="register">
                        <Link to="/register">
                            <span className="icon icon-register"></span> Register
                        </Link>
                    </li>,
                    <li key="enter">
                        <Link to="/login" query={{next: window.location.pathname}}>
                            <span className="icon icon-login"></span> Log in
                        </Link>
                    </li>
                ]
            }

            return (
                <ul className="menu">
                    {activeLinks}
                </ul>
            );
        }
    });
});