define([
    'react',
    'ReactDOM',
    'router',
    'stores/Auth',
    'actions/AuthActions',
    'constants/AuthConstants',
    'components/FieldErrors',
    'utils/Utils'
], function (React, ReactDOM, Router, Auth, AuthActions, AuthConstants, FieldErrors, Utils) {
    return React.createClass({
        mixins : [Router.Navigation],
        getInitialState: function () {
            return {
                errors: {}
            };
        },
        componentWillMount: function() {
            Auth.addEventListener(AuthConstants.REGISTER_SUCCESS, this._onSuccess);
            Auth.addEventListener(AuthConstants.REGISTER_FAIL, this._onFail);
        },
        componentWillUnmount: function() {
            Auth.removeEventListener(AuthConstants.REGISTER_SUCCESS, this._onSuccess);
            Auth.removeEventListener(AuthConstants.REGISTER_FAIL, this._onFail);
        },
        handleSubmit: function(e) {
            e.preventDefault();

            Utils.disable('.form-control');

            var username = ReactDOM.findDOMNode(this.refs.username).value.trim();
            var email = ReactDOM.findDOMNode(this.refs.email).value.trim();
            var password1 = ReactDOM.findDOMNode(this.refs.password1).value.trim();
            var password2 = ReactDOM.findDOMNode(this.refs.password2).value.trim();

            AuthActions.register(username, email, password1, password2);
        },
        _onSuccess: function() {
            noty({text: 'Successfully registered', type: 'success'});
            this.props.history.pushState(null, '/');
        },
        _onFail: function() {
            var errors = Auth.getErrors(AuthConstants.REGISTER_FAIL);
            this.setState({errors: errors}, Utils.enable('.form-control'));
        },
        render: function() {
            return (
                <div>
                    <h1>Register</h1>
                    <form className="form-horizontal" onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <input className="form-control" id="username" ref="username" type="text"
                                   placeholder="Username" />
                            <FieldErrors errors={this.state.errors.username} />
                        </div>
                        <div className="form-group">
                            <input className="form-control" id="email" ref="email" type="email"
                                   placeholder="Email" />
                            <FieldErrors errors={this.state.errors.email} />
                        </div>
                        <div className="form-group">
                            <input className="form-control" id="password1" ref="password1" type="password"
                                   placeholder="Password" />
                            <FieldErrors errors={this.state.errors.password1} />
                        </div>
                        <div className="form-group">
                            <input className="form-control" id="password2" ref="password2" type="password"
                                   placeholder="Confirm password" />
                            <FieldErrors errors={this.state.errors.password2} />
                        </div>
                        <button type="submit" className="btn btn-default pull-right">Submit</button>
                    </form>
                </div>
            );
        }
    });
});