define(['react', 'jquery'], function (React, $) {
    return React.createClass({
        getDefaultProps: function() {
            return {
                errors: []
            }
        },
        render: function() {
            if (!this.props.errors.length) {
                return <span></span>;
            }

            var errors = [];

            for (var i = 0; i < this.props.errors.length; i++) {
                var error = this.props.errors[i];

                if ($.isPlainObject(error)) {
                    errors.push(
                        <li key={i}>{error.message}</li>
                    );
                    continue;
                }

                errors.push(
                    <li key={i}>{error}</li>
                );
            }

            return (
                <span className="help-block alert alert-danger">
                    <ul>
                        {errors}
                    </ul>
                </span>
            );
        }
    });
});