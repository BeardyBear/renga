define([
    'react',
    'ReactDOM',
    'router',
    'stores/Auth',
    'actions/AuthActions',
    'constants/AuthConstants',
    'components/FieldErrors',
    'utils/Utils'
], function (React, ReactDOM, Router, Auth, AuthActions, AuthConstants, FieldErrors, Utils) {
    return React.createClass({
        getInitialState: function () {
            return {
                errors: {}
            };
        },
        componentWillMount: function() {
            Auth.addEventListener(AuthConstants.PASSWORD_RESET_SUCCESS, this._onSuccess);
            Auth.addEventListener(AuthConstants.PASSWORD_RESET_FAIL, this._onFail);
        },
        componentWillUnmount: function() {
            Auth.removeEventListener(AuthConstants.PASSWORD_RESET_SUCCESS, this._onSuccess);
            Auth.removeEventListener(AuthConstants.PASSWORD_RESET_FAIL, this._onFail);
        },
        handleSubmit: function(e) {
            e.preventDefault();

            Utils.disable('.form-control');

            var email = ReactDOM.findDOMNode(this.refs.email).value.trim();

            AuthActions.resetPassword(email);
        },
        _onSuccess: function() {
            this.props.history.pushState(null, '/password/reset/done');
        },
        _onFail: function() {
            var errors = Auth.getErrors(AuthConstants.PASSWORD_RESET_FAIL);
            this.setState({errors: errors}, Utils.enable('.form-control'));
        },
        render: function() {
            return (
                <div>
                    <h1>Password reset</h1>
                    <form className="form-horizontal" onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <input className="form-control" id="email" ref="email" type="email"
                                   placeholder="Your email" />
                            <FieldErrors errors={this.state.errors.email} />
                        </div>
                        <button type="submit" className="btn btn-default pull-right">Submit</button>
                    </form>
                </div>
            );
        }
    });
});