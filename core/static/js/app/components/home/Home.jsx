define([
    'react',
    'router',
    'stores/RengaPaginationStore',
    'constants/RengaConstants',
    'actions/RengaActions',
    'components/browse/RengaBrowserList'
], function (React, Router, RengaPaginationStore, RengaConstants, RengaActions, RengaBrowserList) {
    return React.createClass({
        getInitialState: function() {
            return {
                paginationBundle: {}
            };
        },
        componentWillMount: function() {
            RengaPaginationStore.addEventListener(RengaConstants.PAGE_FETCH_SUCCESS, this._onPageFetch);
        },
        componentDidMount: function() {
            RengaActions.fetchPage();
        },
        componentWillUnmount: function() {
            RengaPaginationStore.removeEventListener(RengaConstants.PAGE_FETCH_SUCCESS, this._onPageFetch);
        },
        _onPageFetch: function () {
            this.setState({paginationBundle: RengaPaginationStore.getPaginationBundle()});
        },
        render: function() {
            return (
                <div>
                    <div className="create">
                        <Router.Link to="/create">
                            <img src="/static/img/create.png" alt="create new renga" />
                        </Router.Link>
                    </div>
                    <h1>Latest rengas</h1>
                    <RengaBrowserList paginationBundle={this.state.paginationBundle} />
                    <div className="minomoto">
                        <img src="/static/img/minomoto.png" className="img-responsive" />
                    </div>
                </div>
            );
        }
    });
});