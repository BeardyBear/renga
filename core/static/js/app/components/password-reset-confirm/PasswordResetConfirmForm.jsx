define([
    'react',
    'ReactDOM',
    'router',
    'stores/Auth',
    'actions/AuthActions',
    'constants/AuthConstants',
    'components/FieldErrors',
    'utils/Utils'
], function (React, ReactDOM, Router, Auth, AuthActions, AuthConstants, FieldErrors, Utils) {
    return React.createClass({
        getInitialState: function () {
            return {
                errors: {}
            };
        },
        componentWillMount: function() {
            Auth.addEventListener(AuthConstants.PASSWORD_RESET_CONFIRM_SUCCESS, this._onSuccess);
            Auth.addEventListener(AuthConstants.PASSWORD_RESET_CONFIRM_FAIL, this._onFail);
        },
        componentWillUnmount: function() {
            Auth.removeEventListener(AuthConstants.PASSWORD_RESET_CONFIRM_SUCCESS, this._onSuccess);
            Auth.removeEventListener(AuthConstants.PASSWORD_RESET_CONFIRM_FAIL, this._onFail);
        },
        handleSubmit: function(e) {
            e.preventDefault();

            Utils.disable('.form-control');

            var new_password1 = ReactDOM.findDOMNode(this.refs.new_password1).value.trim();
            var new_password2 = ReactDOM.findDOMNode(this.refs.new_password2).value.trim();

            AuthActions.confirmPasswordReset(new_password1, new_password2, this.props.params.uidb64, this.props.params.token);
        },
        _onSuccess: function() {
            this.props.history.pushState(null, '/login');
        },
        _onFail: function() {
            var errors = Auth.getErrors(AuthConstants.PASSWORD_RESET_CONFIRM_FAIL);

            if (errors.hasOwnProperty('uidb64') || errors.hasOwnProperty('token')) {
                this.props.history.pushState(null, '/password/reset');
            }

            this.setState({errors: errors}, Utils.enable('.form-control'));
        },
        render: function() {
            return (
                <div>
                    <h1>Set new password</h1>
                    <form className="form-horizontal" onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <input className="form-control" id="new_password1" ref="new_password1" type="password"
                                   placeholder="New password" />
                            <FieldErrors errors={this.state.errors.new_password1} />
                        </div>
                        <div className="form-group">
                            <input className="form-control" id="new_password2" ref="new_password2" type="password"
                                   placeholder="Confirm password" />
                            <FieldErrors errors={this.state.errors.new_password2} />
                        </div>
                        <button type="submit" className="btn btn-default pull-right">Submit</button>
                    </form>
                </div>
            );
        }
    });
});