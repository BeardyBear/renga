define([
    'react',
    'router',
    'jquery',
    'components/menu/Menu',
    'actions/LanguageActions'
], function (React, Router, $, Menu, LanguageActions) {
    return React.createClass({
        componentDidMount: function() {
            var _this = this;
            $(window).resize(function() {
                _this.manageSideClinch();
            });

            this.manageSideClinch();

            LanguageActions.fetch();
        },
        manageSideClinch: function() {
            var rightSel = $('#right');
            rightSel.toggleClass('clinched', rightSel.height() <= $(window).height());
        },
        render: function() {
            return (
                <div className="wrapper">
                    <div className="row-fluid">
                        <div className="center col-lg-4 col-lg-offset-5 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-0 col-xs-12 col-xs-offset-0" id="left">
                            {this.props.children.content || this.props.children}
                        </div>
                        <div className="right col-lg-3 col-md-3 col-sm-4 col-xs-12" id="right">
                            <div id="logo">
                                <Router.IndexLink to="/">
                                    <img src="/static/img/logo.png" alt="Haikai no renga"/>
                                </Router.IndexLink>
                            </div>
                            <Menu />
                            {this.props.children.side}
                        </div>
                    </div>
                </div>
            );
        }
    });
});