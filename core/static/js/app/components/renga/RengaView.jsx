define([
    'react',
    'router',
    'underscore',
    'RengaWS',
    'stores/Auth',
    'stores/RengaStore',
    'actions/RengaActions',
    'constants/RengaConstants',
    'components/renga/VerseForm',
    'components/renga/VerseView',
    'components/renga/Header'
], function (React, Router, _, RengaWS, Auth, RengaStore, RengaActions, RengaConstants, VerseForm, VerseView,
             Header) {
    return React.createClass({
        getInitialState: function() {
            return {
                renga: {
                    verses: []
                }
            };
        },
        componentWillMount: function() {
            RengaStore.addEventListener(RengaConstants.RENGA_FETCH_SUCCESS, this._onRengaChange);
            RengaStore.addEventListener(RengaConstants.RENGA_CHANGE, this._onRengaChange);
            RengaActions.fetchRenga(this.props.params.rengaId);
            RengaWS.init(this.props.params.rengaId);
        },
        componentWillUnmount: function() {
            RengaStore.removeEventListener(RengaConstants.RENGA_FETCH_SUCCESS, this._onRengaChange);
            RengaStore.removeEventListener(RengaConstants.RENGA_CHANGE, this._onRengaChange);
            RengaStore.reset();
            RengaWS.getWs().close();
        },
        _onRengaChange: function() {
            var renga = RengaStore.getRenga();
            this.setState({renga: renga});
        },
        _handleEditClick: function (e) {
            e.preventDefault();

            this.props.history.pushState(null, '/edit/' + this.state.renga.id);
        },
        _handleCloseClick: function (e) {
            e.preventDefault();

            var renga = _.clone(this.state.renga);
            renga.is_closed = 1;

            RengaActions.editRenga(renga);
        },
        _handleOpenClick: function (e) {
            e.preventDefault();

            var renga = _.clone(this.state.renga);
            renga.is_closed = 0;

            RengaActions.editRenga(renga);
        },
        render: function() {
            var renga = this.state.renga;

            var verseForm, warningMessage;

            if (Auth.isAuthenticated() && !renga.is_closed) {
                verseForm = <VerseForm rengaId={renga.id}/>;
            }

            if (!Auth.isAuthenticated() && !renga.is_closed) {
                warningMessage = (
                    <p className="alert alert-info renga-warning">
                        Log in to post your verses
                    </p>
                )
            }

            if (renga.is_closed) {
                warningMessage = (
                    <p className="alert alert-info renga-warning">
                        Renga was closed by its host.
                    </p>
                )
            }

            var actions;
            if (Auth.isAuthenticated() && renga.host && Auth.getUserId() == renga.host.id) {
                var editButton = <button className="btn" onClick={this._handleEditClick}>edit renga</button>;

                var statusToggle;
                if (renga.is_closed) {
                    statusToggle = <button className="btn" onClick={this._handleOpenClick}>open renga</button>;
                } else {
                    statusToggle = <button className="btn" onClick={this._handleCloseClick}>close renga</button>;
                }

                actions = (
                    <div className="owner-actions">
                        {editButton} {statusToggle}
                    </div>
                );
            }

            return (
                <div className="renga">
                    <Header renga={renga} />
                    <div className="verses">
                        <div className="rtop"></div>
                        <div className="rshad">
                            <div className="rpaper">
                                <div className="rpaperw">
                                    <div className="content">
                                        <h1>{renga.name}</h1>
                                        <VerseView verses={renga.verses} />
                                        {verseForm}
                                        {warningMessage}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="rbottom"></div>
                    </div>
                    {actions}
                </div>
            );
        }
    });
});