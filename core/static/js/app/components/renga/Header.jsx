define([
    'react',
    'router',
    'classNames',
    'stores/Auth',
    'stores/RengaStore',
    'actions/RengaActions',
    'constants/RengaConstants',
    'utils/RengaUtils',
    'noty'
], function (React, Router, classNames, Auth, RengaStore, RengaActions, RengaConstants, RengaUtils) {
    return React.createClass({
        mixins: [Router.History],
        propTypes: {
            renga: React.PropTypes.object
        },
        getDefaultProps: function() {
            return {
                renga: []
            }
        },
        componentWillMount: function() {
            RengaStore.addEventListener(RengaConstants.RENGA_JOIN_SUCCESS, this._onJoinSuccess);
            RengaStore.addEventListener(RengaConstants.RENGA_JOIN_FAIL, this._onJoinFail);
            RengaStore.addEventListener(RengaConstants.RENGA_LEAVE_SUCCESS, this._onLeaveSuccess);
            RengaStore.addEventListener(RengaConstants.RENGA_LEAVE_FAIL, this._onLeaveFail);
        },
        componentWillUnmount: function() {
            RengaStore.removeEventListener(RengaConstants.RENGA_JOIN_SUCCESS, this._onJoinSuccess);
            RengaStore.removeEventListener(RengaConstants.RENGA_JOIN_FAIL, this._onJoinFail);
            RengaStore.removeEventListener(RengaConstants.RENGA_LEAVE_SUCCESS, this._onLeaveSuccess);
            RengaStore.removeEventListener(RengaConstants.RENGA_LEAVE_FAIL, this._onLeaveFail);
        },
        _onJoinSuccess: function() {
            noty({text: 'Successfully joined', type: 'success'});
        },
        _onJoinFail: function() {
            noty({text: 'Something went wrong', type: 'error'});
        },
        _onLeaveSuccess: function() {
            noty({text: 'Successfully left', type: 'success'});
        },
        _onLeaveFail: function() {
            noty({text: 'Something went wrong', type: 'error'});
        },
        _handleJoinClick: function (e) {
            e.preventDefault();

            if (!Auth.isAuthenticated()) {
                this.history.pushState(null, '/login', {next: window.location.pathname});
                return;
            }
            RengaActions.joinRenga(this.props.renga.id);
        },
        _handleLeaveClick: function (e) {
            e.preventDefault();

            var userParticipation = RengaUtils.getParticipation(this.props.renga, Auth.getUserId());

            if (userParticipation) {
                RengaActions.leaveRenga(userParticipation.id);
            }
        },
        render: function() {
            var _this = this;
            var renga = this.props.renga;
            var userParticipation = RengaUtils.getParticipation(this.props.renga, Auth.getUserId());
            var buttons = [];

            _.each(renga.participations, function(participation, index) {
                var isUser = userParticipation && userParticipation.user.id == participation.user.id;
                var isOwner = renga.host.id == participation.user.id;

                var leaveLink;
                if (isUser && !isOwner) {
                    leaveLink = <a className="close" href="#" title="Leave" onClick={_this._handleLeaveClick}>&times;</a>;
                }

                var participationClasses = classNames({
                    owner: isOwner,
                    me: isUser && !isOwner
                });
                buttons.push(
                    <li key={participation.id} className={participationClasses}>
                        <Router.Link to={'/profile/' + participation.user.profile}>
                            {participation.user.username}
                        </Router.Link>
                        {leaveLink}
                    </li>
                )
            });

            if (!Auth.isAuthenticated() || !userParticipation) {
                buttons.push(
                    <li className="comein" key="join">
                        <a href="#" onClick={this._handleJoinClick}>
                            join
                        </a>
                    </li>
                );
            }

            return (
                <div className="players">
                    <ul>
                        {buttons}
                        <li className="nojoin">max participants: {renga.max_participants || 'unlimited'}</li>
                    </ul>
                </div>
            )
        }
    });
});