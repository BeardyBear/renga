define([
    'react',
    'ReactDOM',
    'actions/RengaActions',
    'stores/RengaStore',
    'constants/RengaConstants',
    'utils/Utils'
], function (React, ReactDOM, RengaActions, RengaStore, RengaConstants, Utils) {
    return React.createClass({
        getInitialState: function() {
            return {newVerseText: ''};
        },
        handleVerseTextChange: function (e) {
            this.setState({newVerseText: e.target.value});
        },
        componentWillMount: function() {
            RengaStore.addEventListener(RengaConstants.VERSE_POST_SUCCESS, this._onVersePostSuccess);
            RengaStore.addEventListener(RengaConstants.VERSE_POST_FAIL, this._onVersePostFail);
        },
        componentWillUnmount: function() {
            RengaStore.removeEventListener(RengaConstants.VERSE_POST_SUCCESS, this._onVersePostSuccess);
            RengaStore.removeEventListener(RengaConstants.VERSE_POST_FAIL, this._onVersePostFail);
        },
        _onVersePostSuccess: function() {
            this.setState({newVerseText: ''}, Utils.enable('form[name="verseForm"] .form-control'));
        },
        _onVersePostFail: function() {
            Utils.enable('form[name="verseForm"] .form-control');
        },
        handleSubmit: function(e) {
            e.preventDefault();

            Utils.disable('form[name="verseForm"] .form-control');

            var newVerseText = ReactDOM.findDOMNode(this.refs.newVerseText).value.trim();

            RengaActions.postVerse(this.props.rengaId, newVerseText);
        },
        render: function() {
            return (
                <div className="verse-form-container">
                    <form name="verseForm" onSubmit={this.handleSubmit}>
                        <textarea className="form-control" name="verse-textarea" id="message-textarea"
                                  ref="newVerseText" value={this.state.newVerseText} rows="3"
                                  onChange={this.handleVerseTextChange}>
                        </textarea>
                        <button type="submit" className="pull-right"></button>
                    </form>
                </div>
            );
        }
    });
});