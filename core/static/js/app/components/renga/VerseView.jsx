define([
    'react',
    'jquery',
    'classNames',
    'underscore',
    'stores/Auth',
    'actions/RengaActions'
], function (React, $, classNames, _, Auth, RengaActions) {
    return React.createClass({
        propTypes: {
            verses: React.PropTypes.array
        },
        getDefaultProps: function() {
            return {
                verses: []
            }
        },
        componentDidUpdate: function(prevProps, prevState) {
            $('[data-toggle="tooltip"]').tooltip();
        },
        getVerseById: function(verseId) {
            return _.find(this.props.verses, function (verse) {
                return verse.id == verseId;
            });
        },
        hasUpvotedVerse: function(verse) {
            return _.find(verse.upvotes, function (upvote) {
                return upvote.user.id == Auth.getUserId();
            });
        },
        _handleUpvoteClick: function(e) {
            var verseId = e.target.getAttribute('data-verse-id');
            var verse = this.getVerseById(verseId);

            if (!this.hasUpvotedVerse(verse)) {
                RengaActions.upvoteVerse(verseId);
            }
        },
        render: function () {
            var _this = this;
            var verses = [];

            _.each(this.props.verses, function(verse, index) {
                var verseContainerClasses = classNames({
                    even: !(index % 2 == 0),
                    odd: index % 2 == 0
                });
                var upvoteContainerClasses = classNames({
                    hearts: true,
                    click: true,
                    red: _this.hasUpvotedVerse(verse)
                });
                verses.push(
                    <div key={verse.id} className={verseContainerClasses}>
                        <div className={upvoteContainerClasses} data-verse-id={verse.id}
                             onClick={_this._handleUpvoteClick}>{verse.upvotes.length || null}</div>
                        <div data-toggle="tooltip" data-placement="left" title={verse.author.username} className="verse">
                            {verse.text}
                        </div>
                    </div>
                )
            });

            return (
                <div className="verse-container">
                    {verses}
                </div>
            );
        }
    });
});