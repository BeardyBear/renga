define(['react', 'jquery', 'stores/LanguageStore'], function (React, $, LanguageStore) {
    return React.createClass({
        propTypes: {
            onChange: React.PropTypes.func
        },
        getInitialState: function() {
            return {
                languages: []
            };
        },
        componentWillMount: function() {
            LanguageStore.addChangeListener(this._processLanguages);
        },
        componentDidMount: function() {
            var _this = this;
            $("#" + this.props.id).selectOrDie({
                onChange: function() {
                    _this.props.onChange($(this).val());
                },
                prefix: 'Language:'
            });

            this._processLanguages();
        },
        componentWillUnmount: function() {
            LanguageStore.removeChangeListener(this._processLanguages);
        },
        _processLanguages: function() {
            var languages = LanguageStore.getLanguages();

            if (languages.length && this.state.languages != languages) {
                this.setState({languages: languages}, function() {
                    if (!this.props.selected) {
                        this.props.onChange(languages[0].id);
                    }
                });
            }
        },
        componentDidUpdate: function(prevProps, prevState) {
            if ($('.sod_select').length) {
                $("#" + this.props.id).selectOrDie("update");
            }
        },
        render: function() {
            var _this = this;
            var languageOptions = this.state.languages.map(function(language) {
                if (_this.props.selected == language.id) {
                    return <option key={language.id} value={language.id} selected>{language.name}</option>;
                }
                return <option key={language.id} value={language.id}>{language.name}</option>;
            });

            if (!languageOptions.length) {
                languageOptions.push(<option key="0" value="0">Fetching options..</option>);
            }

            return (
                <select className="form-control" id={this.props.id}>
                    {languageOptions}
                </select>
            );
        }
    });
});