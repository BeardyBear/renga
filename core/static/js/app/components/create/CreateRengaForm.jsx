define([
    'react',
    'router',
    'jquery',
    'stores/Auth',
    'actions/RengaActions',
    'constants/RengaConstants',
    'stores/RengaStore',
    'components/LanguageSelect',
    'components/FieldErrors',
    'utils/Utils'
], function (React, Router, $, Auth, RengaActions, RengaConstants, RengaStore, LanguageSelect, FieldErrors, Utils) {
    return React.createClass({
        getInitialState: function () {
            return {
                name: null,
                language: null,
                maxParticipants: 0,
                errors: {}
            };
        },
        componentWillMount: function() {
            if (!Auth.isAuthenticated()) {
                this.props.history.pushState(null, '/login', {next: window.location.pathname});
            }
            RengaStore.addEventListener(RengaConstants.RENGA_CREATE_SUCCESS, this.redirectToRenga);
            RengaStore.addEventListener(RengaConstants.RENGA_CREATE_FAIL, this._onFail);
        },
        componentWillUnmount: function() {
            RengaStore.removeEventListener(RengaConstants.RENGA_CREATE_SUCCESS, this.redirectToRenga);
            RengaStore.removeEventListener(RengaConstants.RENGA_CREATE_FAIL, this._onFail);

            RengaStore.reset();
        },
        componentDidMount: function() {
            var _this = this;
            $('#max-participants').selectOrDie({
                onChange: function() {
                    _this.setState({maxParticipants: $(this).val()});
                },
                prefix: 'Max participants:'
            });
        },
        redirectToRenga: function() {
            var createdRenga = RengaStore.getRenga();
            this.props.history.pushState(null, '/renga/' + createdRenga.id);
        },
        _onFail: function() {
            this.setState({errors: RengaStore.getErrors(RengaConstants.RENGA_CREATE_FAIL)}, Utils.enable('.form-control'));
        },
        handleSubmit: function(e) {
            e.preventDefault();

            Utils.disable('.form-control');

            var renga = {
                name: this.state.name,
                language: this.state.language,
                max_participants: this.state.maxParticipants,
                host: Auth.getUserId()
            };
            RengaActions.createRenga(renga);
        },
        _onNameChange: function(e) {
            this.setState({name: e.target.value});
        },
        _onLanguageChange: function(languageId) {
            this.setState({language: languageId});
        },
        render: function() {
            var maxParticipantsSelect, maxParticipantsSelectOptions = [];

            for (var i=0; i <= 10; i++) {
                if (i == 0) {
                    maxParticipantsSelectOptions.push(
                        <option key={i} value={i}>Unlimited</option>
                    );
                    continue;
                }

                maxParticipantsSelectOptions.push(
                    <option key={i} value={i}>{i}</option>
                );
            }

            maxParticipantsSelect = (
                <select className="form-control" id="max-participants">
                    {maxParticipantsSelectOptions}
                </select>
            );

            return (
                <div>
                    <h1>Create new renga</h1>
                    <form className="form-horizontal" onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <input className="form-control" id="name" ref="name" type="text" placeholder="Name"
                                   value={this.state.name} onChange={this._onNameChange} />
                            <FieldErrors errors={this.state.errors.name} />
                        </div>
                        <div className="form-group">
                            <LanguageSelect id="language" onChange={this._onLanguageChange} selected={this.state.selected} />
                        </div>
                        <div className="form-group">
                            {maxParticipantsSelect}
                            <FieldErrors errors={this.state.errors.max_participants} />
                        </div>
                        <button type="submit" className="btn btn-default pull-right">Submit</button>
                    </form>
                </div>
            );
        }
    });
});