define(['underscore', 'AppDispatcher'], function (_, AppDispatcher) {
    var ws = null;

    var RengaWS = {
        init: function(rengaId) {
            ws = new WebSocket("ws://" + window.location.hostname + ":8888/sock/" + rengaId + "/");

            ws.onmessage = function(event) {
                var data = JSON.parse(event.data);
                if (_.isObject(data)) {
                    AppDispatcher.dispatch({
                        actionType: data.actionType,
                        data: data.data
                    });
                }
            };
        },
        getWs: function() {
            return ws;
        }
    };

    return RengaWS;
});