define([
    'react',
    'ReactDOM',
    'router',
    'History',
    'jquery',
    'underscore',
    'Cookies',
    'stores/Auth',
    'actions/AuthActions',
    'constants/AuthConstants',
    'components/RootComponent',
    'components/home/Home',
    'components/about/About',
    'components/login/LoginForm',
    'components/register/RegisterForm',
    'components/logout/Logout',
    'components/create/CreateRengaForm',
    'components/browse/RengaBrowser',
    'components/renga/RengaView',
    'components/edit/EditRengaForm',
    'components/profile/Profile',
    'components/edit-profile/EditProfileForm',
    'components/password-change/PasswordChangeForm',
    'components/password-reset/PasswordResetForm',
    'components/password-reset-done/PasswordResetDone',
    'components/password-reset-confirm/PasswordResetConfirmForm',
    'components/chat/Chat',
    'noty',
    'bootstrap',
    'SelectOrDie'
], function (React, ReactDOM, Router, History, $, _, Cookies, Auth, AuthActions, AuthConstants, RootComponent, Home,
             About, LoginForm, RegisterForm, Logout, CreateRengaForm, RengaBrowser, RengaView, EditRengaForm, Profile,
             EditProfileForm, PasswordChangeForm, PasswordResetForm, PasswordResetDone, PasswordResetConfirmForm,
             Chat) {
    $.noty.defaults = _.extend($.noty.defaults, {
        layout: 'bottomRight',
        theme: 'relax',
        timeout: 3000,
        animation: {
            open: 'animated bounceInRight',
            close: 'animated bounceOutRight'
        }
    });

    var history = History.createHistory();

    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRFToken', Cookies.get('csrftoken'));
        }
    });

    Auth.addEventListener(AuthConstants.JWT_REFRESH_SUCCESS, function () {
        $.ajaxSetup({
            beforeSend: function (xhr) {
                if (Auth.getToken()) {
                    xhr.setRequestHeader('Authorization', 'JWT ' + Auth.getToken());
                }
                xhr.setRequestHeader('X-CSRFToken', Cookies.get('csrftoken'));
            }
        });
    });

    Auth.addEventListener(AuthConstants.JWT_TOKEN_EXPIRED, function () {
        // TODO: get current url, pass as "next" query parameter
        history.pushState(null, '/login');
    });

    if (Auth.isAuthenticated()) {
        AuthActions.refreshToken(Auth.getToken());
    }

    var Route = Router.Route;

    var routes = (
        <Route name="app" path="/" component={RootComponent}>
            <Router.IndexRoute component={Home}/>
            <Route name="about" path="about" component={About}/>
            <Route name="login" path="login" component={LoginForm}/>
            <Route name="register" path="register" component={RegisterForm}/>
            <Route name="logout" path="logout" component={Logout}/>
            <Route name="create" path="create" component={CreateRengaForm}/>
            <Route name="edit" path="/edit/:rengaId" component={EditRengaForm}/>
            <Route name="browse" path="browse" component={RengaBrowser}/>
            <Route name="renga" path="/renga/:rengaId" components={{content: RengaView, side: Chat}}/>
            <Route name="profile" path="/profile/:profileId" component={Profile}/>
            <Route name="edit-profile" path="/profile/:profileId/edit" component={EditProfileForm}/>
            <Route name="password-change" path="/password/change" component={PasswordChangeForm}/>
            <Route name="password-reset" path="/password/reset" component={PasswordResetForm}/>
            <Route name="password-reset-done" path="/password/reset/done" component={PasswordResetDone}/>
            <Route name="password-reset-confirm" path="/password/reset/confirm/:uidb64/:token"
                   component={PasswordResetConfirmForm}/>
        </Route>
    );

    return {
        initialize: function () {
            ReactDOM.render(<Router.Router history={history} routes={routes}/>, document.getElementById('app'));
        }
    }
});
