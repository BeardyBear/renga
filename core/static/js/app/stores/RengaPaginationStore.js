define([
    'jquery',
    'EventEmitter',
    'AppDispatcher',
    'constants/RengaConstants'
], function ($, EventEmitter, AppDispatcher, RengaConstants) {
    var _errors = {};

    function setErrors(constant, errors) {
        _errors[constant] = errors;
    }

    var _paginationBundle = {};

    function setPaginationBundle(paginationBundle) {
        _paginationBundle = paginationBundle;
    }

    var RengaPaginationStore = _.extend({}, EventEmitter.prototype, {
        getPaginationBundle: function() {
            return _paginationBundle;
        },
        emitEvent: function(event) {
            this.emit(event);
        },
        addEventListener: function(event, callback) {
            this.on(event, callback);
        },
        removeEventListener: function(event, callback) {
            this.removeListener(event, callback);
        },
        reset: function() {
            _paginationBundle = {};
        },
        getErrors: function(constant) {
            if (_errors.hasOwnProperty(constant)) {
                return _errors[constant];
            }

            return {};
        }
    });

    AppDispatcher.register(function(action) {
        switch(action.actionType) {
            case RengaConstants.PAGE_FETCH_SUCCESS:
                setPaginationBundle(action.data);
                RengaPaginationStore.emitEvent(action.actionType);
                break
        }
    });

    return RengaPaginationStore;
});