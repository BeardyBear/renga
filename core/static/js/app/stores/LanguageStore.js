define([
    'jquery',
    'EventEmitter',
    'AppDispatcher',
    'constants/LanguageConstants'
], function ($, EventEmitter, AppDispatcher, LanguageConstants) {
    var CHANGE_EVENT = 'change';

    var _languages = [];

    function setLanguages(languages) {
        _languages = languages;
    }

    var LanguageStore = _.extend({}, EventEmitter.prototype, {
        getLanguages: function() {
            return _languages;
        },
        emitChange: function () {
            this.emit(CHANGE_EVENT);
        },
        addChangeListener: function (callback) {
            this.on(CHANGE_EVENT, callback);
        },
        removeChangeListener: function (callback) {
            this.removeListener(CHANGE_EVENT, callback);
        }
    });

    AppDispatcher.register(function(action) {
        switch(action.actionType) {
            case LanguageConstants.LANGUAGES_FETCH:
                setLanguages(action.languages);
                LanguageStore.emitChange();
                break
        }
    });

    return LanguageStore;
});