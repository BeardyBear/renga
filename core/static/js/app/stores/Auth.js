define([
    'jquery',
    'AppDispatcher',
    'Cookies',
    'JWT',
    'EventEmitter',
    'constants/AuthConstants'
], function ($, AppDispatcher, Cookies, JWT, EventEmitter, AuthConstants) {
    var _errors = {};

    function setErrors(constant, errors) {
        _errors[constant] = errors || {};
    }

    var Auth = _.extend({}, EventEmitter.prototype, {
        isAuthenticated: function () {
            var jwt_token = Cookies.get('jwt_token');

            if (!jwt_token) {
                return false;
            }

            var isExpired = JWT.isTokenExpired(jwt_token);

            if (isExpired) {
                clearCookies();
                this.emitEvent(AuthConstants.JWT_TOKEN_EXPIRED);
            }

            return !isExpired;
        },
        getToken: function () {
            return Cookies.get('jwt_token');
        },
        getUser: function () {
            return Cookies.getJSON('user') || {};
        },
        getUserId: function () {
            return this.getUser().id
        },
        emitEvent: function(event) {
            this.emit(event);
        },
        addEventListener: function(event, callback) {
            this.on(event, callback);
        },
        removeEventListener: function(event, callback) {
            this.removeListener(event, callback);
        },
        getErrors: function(constant) {
            if (_errors.hasOwnProperty(constant)) {
                return _errors[constant];
            }

            return {};
        }
    });

    function logIn(jwt_token) {
        var userData = JWT.decodeToken(jwt_token);
        console.log(userData);
        Cookies.set('user', userData);
        Cookies.set('jwt_token', jwt_token);
        console.log("EXPIRES: " + JWT.getTokenExpirationDate(jwt_token));
    }

    function clearCookies() {
        Cookies.remove('jwt_token');
        Cookies.remove('user');
    }

    AppDispatcher.register(function(action) {
        switch(action.actionType) {
            case AuthConstants.REGISTER_SUCCESS:
            case AuthConstants.LOG_IN_SUCCESS:
            case AuthConstants.JWT_REFRESH_SUCCESS:
                logIn(action.data.token);
                Auth.emitEvent(action.actionType);
                break;

            case AuthConstants.REGISTER_FAIL:
            case AuthConstants.PASSWORD_CHANGE_FAIL:
            case AuthConstants.PASSWORD_RESET_FAIL:
            case AuthConstants.PASSWORD_RESET_CONFIRM_FAIL:
            case AuthConstants.LOG_IN_FAIL:
                setErrors(action.actionType, action.data);
                Auth.emitEvent(action.actionType);
                break;

            case AuthConstants.PASSWORD_CHANGE_SUCCESS:
            case AuthConstants.PASSWORD_RESET_SUCCESS:
            case AuthConstants.PASSWORD_RESET_CONFIRM_SUCCESS:
                Auth.emitEvent(action.actionType);
                break;

            case AuthConstants.LOG_OUT:
                clearCookies();
                Auth.emitEvent(action.actionType);
                break;

            case AuthConstants.JWT_REFRESH_FAIL:
                setErrors(action.actionType, action.data);
                clearCookies();
                Auth.emitEvent(action.actionType);
                break;
        }
    });

    return Auth;
});