define([
    'jquery',
    'EventEmitter',
    'AppDispatcher',
    'constants/RengaConstants'
], function ($, EventEmitter, AppDispatcher, RengaConstants) {
    var _errors = {};

    function setErrors(constant, errors) {
        _errors[constant] = errors;
    }

    var _renga_default = {
        verses: []
    };
    var _renga = _renga_default;

    function setRenga(renga) {
        _renga = renga;
    }

    var RengaStore = _.extend({}, EventEmitter.prototype, {
        getRenga: function() {
            return _renga;
        },
        emitEvent: function(event) {
            this.emit(event);
        },
        addEventListener: function(event, callback) {
            this.on(event, callback);
        },
        removeEventListener: function(event, callback) {
            this.removeListener(event, callback);
        },
        reset: function() {
            _renga = _renga_default;
        },
        getErrors: function(constant) {
            if (_errors.hasOwnProperty(constant)) {
                return _errors[constant];
            }

            return {};
        }
    });

    AppDispatcher.register(function(action) {
        switch(action.actionType) {
            case RengaConstants.RENGA_CREATE_SUCCESS:
            case RengaConstants.RENGA_EDIT_SUCCESS:
            case RengaConstants.RENGA_FETCH_SUCCESS:
            case RengaConstants.RENGA_CHANGE:
                setRenga(action.data);
                RengaStore.emitEvent(action.actionType);
                break;

            case RengaConstants.VERSE_POST_SUCCESS:
            case RengaConstants.VERSE_UPVOTE_SUCCESS:
            case RengaConstants.RENGA_JOIN_SUCCESS:
            case RengaConstants.RENGA_LEAVE_SUCCESS:
            case RengaConstants.CHAT_MESSAGE_POST_SUCCESS:
                RengaStore.emitEvent(action.actionType);
                break;

            case RengaConstants.RENGA_CREATE_FAIL:
            case RengaConstants.RENGA_EDIT_FAIL:
            case RengaConstants.VERSE_POST_FAIL:
            case RengaConstants.VERSE_UPVOTE_FAIL:
            case RengaConstants.RENGA_JOIN_FAIL:
            case RengaConstants.RENGA_LEAVE_FAIL:
                setErrors(action.actionType, action.data);
                RengaStore.emitEvent(action.actionType);
                break;
        }

        console.log(action.actionType);
    });

    return RengaStore;
});