define([
    'jquery',
    'EventEmitter',
    'AppDispatcher',
    'constants/ProfileConstants'
], function ($, EventEmitter, AppDispatcher, ProfileConstants) {
    var _errors = {};

    function setErrors(constant, errors) {
        _errors[constant] = errors;
    }

    var _profile = {};

    function setProfile(profile) {
        _profile = profile;
    }

    var ProfileStore = _.extend({}, EventEmitter.prototype, {
        getProfile: function() {
            return _profile;
        },
        emitEvent: function(event) {
            this.emit(event);
        },
        addEventListener: function(event, callback) {
            this.on(event, callback);
        },
        removeEventListener: function(event, callback) {
            this.removeListener(event, callback);
        },
        reset: function() {
            _profile = {};
        },
        getErrors: function(constant) {
            if (_errors.hasOwnProperty(constant)) {
                return _errors[constant];
            }

            return {};
        }
    });

    AppDispatcher.register(function(action) {
        switch(action.actionType) {
            case ProfileConstants.PROFILE_FETCH_SUCCESS:
            case ProfileConstants.PROFILE_EDIT_SUCCESS:
                setProfile(action.data);
                ProfileStore.emitEvent(action.actionType);
                break;

            case ProfileConstants.PROFILE_EDIT_FAIL:
                setErrors(action.actionType, action.data);
                ProfileStore.emitEvent(action.actionType);
                break;
        }
    });

    return ProfileStore;
});