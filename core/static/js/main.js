require.config({
    baseUrl: '/static/',
    paths: {
        app: 'js/app',
        components: 'js/app/components',
        constants: 'js/app/constants',
        actions: 'js/app/actions',
        stores: 'js/app/stores',
        utils: 'js/app/utils',
        jquery: 'bower_components/jquery/dist/jquery.min',
        EventEmitter: 'bower_components/eventemitter2/lib/eventemitter2',
        bootstrap: 'bower_components/bootstrap/dist/js/bootstrap.min',
        underscore: 'bower_components/underscore/underscore-min',
        flux: 'bower_components/flux/dist/Flux',
        react: 'bower_components/react/react',
        ReactDOM: 'bower_components/react/react-dom',
        Cookies: 'bower_components/js-cookie/src/js.cookie',
        History: 'js/history/umd/History',
        router: 'bower_components/react-router/ReactRouter',
        jsx: 'bower_components/react/JSXTransformer',
        noty: 'bower_components/noty/js/noty/packaged/jquery.noty.packaged',
        AppDispatcher: 'js/app/AppDispatcher',
        RengaWS: 'js/app/RengaWS',
        classNames: 'bower_components/classnames/index',
        JWT: 'js/app/JWT',
        SelectOrDie: 'bower_components/SelectOrDie/_src/selectordie',
        mCustomScrollbar: 'bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar'
    }
});

require([
    'app/App'
], function (App) {
    App.initialize();
});