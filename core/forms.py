from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.core.validators import validate_email


class RegistrationForm(UserCreationForm):
    email = forms.EmailField(required=True, validators=[validate_email])

    def save_user(self):
        user = super(RegistrationForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        user.save()

        return user
