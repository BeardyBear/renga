from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm, PasswordResetForm, SetPasswordForm
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.views.decorators.http import require_POST
from django.conf import settings
from rest_framework_jwt.utils import jwt_encode_handler
from core.forms import RegistrationForm
from core.serializers import UserSerializer
from core.utils.views import json_response
from core.utils.jwt import jwt_payload_handler


@require_POST
def register(request):
    form = RegistrationForm(request.POST or None)

    if form.is_valid():
        user = form.save_user()
        success_email = render_to_string('email/registration_success.html', {'user': user})
        send_mail('Registration success', success_email, settings.DEFAULT_FROM_EMAIL, [user.email])

        jwt_payload = jwt_payload_handler(user)
        jwt_token = jwt_encode_handler(jwt_payload)
        return json_response({'token': jwt_token})

    errors_json = form.errors.as_json()
    return json_response(errors_json, status=400)


@require_POST
@login_required
def password_change(request):
    form = PasswordChangeForm(request.user, request.POST or None)

    if form.is_valid():
        user = form.save()
        serialized_user = UserSerializer(user).data
        return json_response(serialized_user)

    errors_json = form.errors.as_json()
    return json_response(errors_json, status=400)


@require_POST
def password_reset(request):
    form = PasswordResetForm(request.POST or None)

    if form.is_valid():
        form.save(request=request, email_template_name='email/password_reset_email.html',
                  from_email=settings.DEFAULT_FROM_EMAIL)
        return json_response()

    errors_json = form.errors.as_json()
    return json_response(errors_json, status=400)


@require_POST
def password_reset_confirm(request):
    uidb64 = request.POST.get('uidb64', None)
    token = request.POST.get('token', None)

    try:
        # urlsafe_base64_decode() decodes to bytestring on Python 3
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        errors = {'uidb64': [{'message': 'Invalid uidb64', 'code': 'invalid_uidb64'}]}
        return json_response(errors, status=400)

    if not default_token_generator.check_token(user, token):
        errors = {'token': [{'message': 'Invalid token', 'code': 'invalid_token'}]}
        return json_response(errors, status=400)

    form = SetPasswordForm(user, request.POST)
    if form.is_valid():
        user = form.save()
        serialized_user = UserSerializer(user).data
        return json_response(serialized_user)
    errors_json = form.errors.as_json()
    return json_response(errors_json, status=400)
