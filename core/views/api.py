from rest_framework import generics, viewsets, mixins
from core.models import Language, Renga, Verse, RengaParticipation, VerseUpvote, Profile, ChatMessage
from core.serializers import LanguageSerializer, RengaSerializer, VerseSerializer, RengaVerboseSerializer, \
    RengaParticipationSerializer, VerseUpvoteSerializer, ProfileVerboseSerializer, ProfileSerializer, \
    ChatMessageSerializer, ChatMessageVerboseSerializer
from core.utils.rest import PageNumberPagination


class LanguageListView(generics.ListAPIView):
    queryset = Language.objects.all()
    serializer_class = LanguageSerializer


class RengaListView(generics.ListAPIView):
    serializer_class = RengaVerboseSerializer
    pagination_class = PageNumberPagination
    paginate_by = 30

    def get_queryset(self):
        queryset = Renga.objects.all()

        name_filter = self.request.query_params.get('name', None)
        if name_filter:
            queryset = queryset.filter(name__icontains=name_filter)

        language_filter = self.request.query_params.get('language', None)
        if language_filter:
            queryset = queryset.filter(language__id=int(language_filter))

        return queryset


class RengaCreateView(generics.CreateAPIView):
    queryset = Renga.objects.all()
    serializer_class = RengaSerializer


class RengaRetrieveView(generics.RetrieveAPIView):
    queryset = Renga.objects.all()
    serializer_class = RengaVerboseSerializer


class RengaUpdateView(generics.UpdateAPIView):
    queryset = Renga.objects.all()
    serializer_class = RengaSerializer


class VerseCreateView(generics.CreateAPIView):
    queryset = Verse.objects.all()
    serializer_class = VerseSerializer


class RengaParticipationCreateView(generics.CreateAPIView):
    queryset = RengaParticipation.objects.all()
    serializer_class = RengaParticipationSerializer


class RengaParticipationDestroyView(generics.DestroyAPIView):
    queryset = RengaParticipation.objects.all()
    serializer_class = RengaParticipationSerializer


class VerseUpvoteCreateView(generics.CreateAPIView):
    queryset = VerseUpvote.objects.all()
    serializer_class = VerseUpvoteSerializer


class ProfileRetrieveView(generics.RetrieveAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileVerboseSerializer


class ProfileUpdateView(generics.UpdateAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer


class ChatMessageListView(generics.ListAPIView):
    serializer_class = ChatMessageVerboseSerializer

    def get_queryset(self):
        queryset = ChatMessage.objects.all()

        renga_filter = self.request.query_params.get('renga', None)
        if renga_filter:
            queryset = queryset.filter(renga=int(renga_filter))

        return queryset


class ChatMessageCreateView(generics.CreateAPIView):
    queryset = ChatMessage.objects.all()
    serializer_class = ChatMessageSerializer
