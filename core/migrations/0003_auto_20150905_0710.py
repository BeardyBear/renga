# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0002_verseupvote'),
    ]

    operations = [
        migrations.CreateModel(
            name='RengaParticipation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_banned', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('renga', models.ForeignKey(related_name='participations', to='core.Renga')),
                ('user', models.ForeignKey(related_name='participations', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterField(
            model_name='verseupvote',
            name='verse',
            field=models.ForeignKey(related_name='upvoes', to='core.Verse'),
        ),
    ]
