# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20150905_0710'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='rengaparticipation',
            name='is_banned',
        ),
        migrations.AlterField(
            model_name='verseupvote',
            name='verse',
            field=models.ForeignKey(related_name='upvotes', to='core.Verse'),
        ),
    ]
