# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_auto_20151028_1033'),
    ]

    operations = [
        migrations.AlterField(
            model_name='renga',
            name='max_participants',
            field=models.PositiveIntegerField(),
        ),
    ]
