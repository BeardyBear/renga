# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20150910_1045'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='rengaparticipation',
            unique_together=set([('user', 'renga')]),
        ),
    ]
