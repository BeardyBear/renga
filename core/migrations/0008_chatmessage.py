# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_renga_max_participants'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChatMessage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField()),
                ('renga', models.ForeignKey(related_name='chat_messages', to='core.Renga')),
                ('user', models.ForeignKey(related_name='chat_messages', to='core.Verse')),
            ],
        ),
    ]
