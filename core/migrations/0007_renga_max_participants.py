# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_profile'),
    ]

    operations = [
        migrations.AddField(
            model_name='renga',
            name='max_participants',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
