from calendar import timegm
from rest_framework_jwt.settings import api_settings
from core.serializers import UserVerboseSerializer
from datetime import datetime


def jwt_payload_handler(user):
    payload = {
        'exp': datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA
    }
    serialized_user = UserVerboseSerializer(user).data
    payload.update(serialized_user)

    if api_settings.JWT_ALLOW_REFRESH:
        payload['orig_iat'] = timegm(
            datetime.utcnow().utctimetuple()
        )

    return payload
