import json
from django.http import HttpResponse
from core.utils.common import is_json


def json_response(response_body='{}', status=200):
    if is_json(response_body):
        return HttpResponse(response_body, content_type="application/json", status=status)
    return HttpResponse(json.dumps(response_body), content_type="application/json", status=status)
