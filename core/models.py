# -*- coding: utf-8 -*-
from annoying.fields import AutoOneToOneField
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _
import redis
from rest_framework.renderers import JSONRenderer
from core.utils.common import import_to_python


class SerializableQuerySet(models.QuerySet):
    def to_dict(self, serializer=None):
        if not hasattr(self.model, 'get_serializer'):
            raise NotImplementedError('Model must implement get_serializer method')

        serializer = serializer or self.model.get_serializer()
        return serializer(self, many=True).data

    def to_json(self, serializer=None):
        return JSONRenderer().render(self.to_dict(serializer))


class SerializableMixin(object):
    @property
    def serializer(self):
        serializer = self.get_serializer()
        return serializer(self)

    @classmethod
    def get_serializer(cls):
        import_path = '{0}.serializers.{1}Serializer'.format(cls._meta.app_label, cls.__name__)
        return import_to_python(import_path)

    def to_dict(self, serializer=None):
        return dict(self.to_ordered_dict(serializer))

    def to_ordered_dict(self, serializer=None):
        serializer = serializer or self.get_serializer()
        return serializer(self).data

    def to_json(self, serializer=None):
        return JSONRenderer().render(self.to_ordered_dict(serializer))


class Language(models.Model):
    name = models.CharField(_('Name'), unique=True, max_length=50, null=False)
    code = models.CharField(_('Code'), unique=True, max_length=50, null=False)

    def __unicode__(self):
        return u'%s [%s]' % (_(self.name), self.code)

    class Meta:
        verbose_name = _('language')
        verbose_name_plural = _('languages')
        ordering = ('code',)

    @cached_property
    def short_name(self):
        return self.code


class Renga(models.Model, SerializableMixin):
    name = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    is_closed = models.BooleanField(default=False)
    language = models.ForeignKey(Language)
    host = models.ForeignKey(User, related_name="hosted_rengas")
    max_participants = models.PositiveIntegerField()

    def __init__(self, *args, **kwargs):
        super(Renga, self).__init__(*args, **kwargs)
        self.redis_client = redis.StrictRedis()

    def __unicode__(self):
        return self.name

    @cached_property
    def channel(self):
        return 'renga_%d' % self.pk

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse

        return reverse('renga', args=[self.pk])

    def publish_to_client(self, payload):
        self.redis_client.publish(self.channel, JSONRenderer().render(payload))

    def notify_changes(self):
        from core.serializers import RengaVerboseSerializer

        payload = {
            'actionType': 'RENGA_CHANGE',
            'data': self.to_dict(RengaVerboseSerializer)
        }

        self.publish_to_client(payload)


class RengaParticipation(models.Model):
    user = models.ForeignKey(User, related_name="participations")
    renga = models.ForeignKey(Renga, related_name="participations")
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('user', 'renga')


class Verse(models.Model):
    renga = models.ForeignKey(Renga, related_name='verses')
    author = models.ForeignKey(User)
    text = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now_add=True)


class Profile(models.Model):
    user = AutoOneToOneField(User)
    rating = models.IntegerField(default=0)
    about = models.TextField(null=True, blank=True)


class VerseUpvote(models.Model):
    verse = models.ForeignKey(Verse, related_name='upvotes')
    user = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now_add=True)


class ChatMessage(models.Model):
    user = models.ForeignKey(User, related_name='chat_messages')
    renga = models.ForeignKey(Renga, related_name='chat_messages')
    text = models.TextField(blank=False)


@receiver(post_save, sender=Renga)
def renga_post_save(sender, instance, created, **kwargs):
    if created:
        RengaParticipation.objects.create(user=instance.host, renga=instance)
    instance.notify_changes()


@receiver(post_save, sender=RengaParticipation)
def renga_participation_post_save(sender, instance, created, **kwargs):
    instance.renga.notify_changes()


@receiver(post_save, sender=Verse)
def verse_post_save(sender, instance, created, **kwargs):
    instance.renga.notify_changes()


@receiver(post_save, sender=VerseUpvote)
def verse_upvote_post_save(sender, instance, created, **kwargs):
    instance.verse.renga.notify_changes()


@receiver(post_delete, sender=Renga)
def renga_post_delete(sender, instance, using, **kwargs):
    instance.notify_changes()


@receiver(post_delete, sender=RengaParticipation)
def renga_participation_post_delete(sender, instance, using, **kwargs):
    instance.renga.notify_changes()


@receiver(post_delete, sender=Verse)
def verse_post_delete(sender, instance, using, **kwargs):
    instance.renga.notify_changes()


@receiver(post_save, sender=ChatMessage)
def chat_message_post_save(sender, instance, created, **kwargs):
    instance.renga.notify_changes()


@receiver(post_save, sender=VerseUpvote)
def update_user_rating(**kwargs):
    instance = kwargs.get('instance')
    instance.verse.author.profile.rating += 1
    instance.verse.author.profile.save()
