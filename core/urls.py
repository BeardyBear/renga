from django.conf.urls import patterns, url
from core.views.api import LanguageListView, VerseCreateView, RengaListView, RengaCreateView, RengaRetrieveView, \
    ProfileRetrieveView, RengaUpdateView, ProfileUpdateView, ChatMessageCreateView, ChatMessageListView, \
    VerseUpvoteCreateView, RengaParticipationCreateView, RengaParticipationDestroyView


urlpatterns = patterns('',
    url(r'^$', 'core.views.app.index', name='index'),
    url(r'^auth/register/$', 'core.views.auth.register', name='register'),
    url(r'^auth/password/change/$', 'core.views.auth.password_change', name='password_change'),
    url(r'^auth/password/reset/$', 'core.views.auth.password_reset', name='password_reset'),
    url(r'^auth/password/reset/confirm/$', 'core.views.auth.password_reset_confirm', name='password_reset_confirm'),
)


API_URLS = patterns('',
    url(r'^api/languages/$', LanguageListView.as_view(), name='list-languages'),
    url(r'^api/renga/$', RengaListView.as_view(), name='list-renga'),
    url(r'^api/renga/create/$', RengaCreateView.as_view(), name='create-renga'),
    url(r'^api/renga/(?P<pk>[0-9]+)/$', RengaRetrieveView.as_view(), name='retrieve-renga'),
    url(r'^api/renga/(?P<pk>[0-9]+)/update/$', RengaUpdateView.as_view(), name='update-renga'),
    url(r'^api/profile/(?P<pk>[0-9]+)/$', ProfileRetrieveView.as_view(), name='retrieve-profile'),
    url(r'^api/profile/(?P<pk>[0-9]+)/update/$', ProfileUpdateView.as_view(), name='update-profile'),
    url(r'^api/verse/create/$', VerseCreateView.as_view(), name='create-verse'),
    url(r'^api/chat/$', ChatMessageListView.as_view(), name='list-chat-message'),
    url(r'^api/chat/create/$', ChatMessageCreateView.as_view(), name='create-chat-message'),
    url(r'^api/upvote/create/$', VerseUpvoteCreateView.as_view(), name='create-upvote'),
    url(r'^api/participation/create/$', RengaParticipationCreateView.as_view(), name='create-participation'),
    url(r'^api/participation/destroy/(?P<pk>[0-9]+)/$', RengaParticipationDestroyView.as_view(), name='destroy-participation'),
)


urlpatterns += API_URLS
