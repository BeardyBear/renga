# -*- coding: utf-8 -*-
import multiprocessing

bind = '127.0.0.1:8282'
workers = multiprocessing.cpu_count() * 2 + 1
reload = True
preload_app = True
chdir = '/home/beardy/projects/renga'
pythonpath = '/home/beardy/.envs/renga/bin/python'
raw_env = [
    'DJANGO_SETTINGS_MODULE=renga.settings',
    'LANG=ru_RU.UTF-8',
    'LC_ALL=ru_RU.UTF-8',
    'LC_LANG=ru_RU.UTF-8'
]
user = 'beardy'
group = 'beardy'
max_requests = 1
